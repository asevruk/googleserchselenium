import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.textToBePresentInElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.urlContains;
import static core.CustomConditions.*;
import static core.CustomConditions.minimumSizeOf;


/**
 * Created by asevruk on 4/12/2016.
 */
public class GoogleSearch {
    String result = ".srg>.g";
    static WebDriver driver;
    static WebDriverWait wait;


    @BeforeClass
    public static void setUp() throws Exception {
        driver = new FirefoxDriver();
        wait = new WebDriverWait(driver, 6);
    }

    @AfterClass
    public static void teardown() {
        driver.quit();
    }

    @Test
    public void testSearch() {
        search("Selenium automates browsers");
        followLink(0);
        wait.until(urlContains("http://www.seleniumhq.org/"));
    }

    @Test
    public void testSearchCommonFlow() {
        search("Selenium automates browsers");

        wait.until(sizeOf(By.cssSelector(result), 10));
        wait.until(textToBePresentInElementLocated(By.cssSelector(result + ":nth-child(1)"), "Selenium automates browsers"));

        followLink(0);

        wait.until(urlContains("http://www.seleniumhq.org/"));
    }

    // i didn't understand what exactly i should change in this method
    public void followLink(int index) {
        wait.until(minimumSizeOf(By.cssSelector(result), index + 1));
        driver.findElements(By.cssSelector(result + " .r")).get(index).click();
    }

    private void search(String text) {
        driver.get("http://google.com/ncr");
        driver.findElement(By.name("q")).clear();
        driver.findElement(By.name("q")).sendKeys(text + Keys.ENTER);
    }




}
